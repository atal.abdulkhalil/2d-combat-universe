﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Register : MonoBehaviour
{
    public InputField mailField;
    public InputField passwordField;

    public Button submitButton;
    public void goBack()
    {
        SceneManager.LoadScene(0);
    }

    public void CallRegister()
    {
        StartCoroutine(RegisterPlayer());
    }

    IEnumerator RegisterPlayer()
    {
        WWWForm form = new WWWForm();
        form.AddField("mail", mailField.text);
        form.AddField("password", passwordField.text);
        // lokaal:
        //WWW www = new WWW("https://jaspercoppens.ikdoeict.be/register.php", form);

        UnityWebRequest www = UnityWebRequest.Post("https://combatuniversephp.azurewebsites.net", form);
        www.chunkedTransfer = false;
        yield return www.SendWebRequest();

         if (www.isNetworkError || www.isHttpError)
         {
             Debug.Log(www.error);
         }
         else
         {
            SceneManager.LoadScene(0);
        }
    }

    public void VerifyInputs() {
        submitButton.interactable = (mailField.text.Length > 10 && passwordField.text.Length >= 8);
    }
}
