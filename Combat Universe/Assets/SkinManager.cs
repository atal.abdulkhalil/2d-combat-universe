﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
#if UNITY_EDITOR
using UnityEditor;
#endif


public class SkinManager : MonoBehaviour
{
    public SpriteRenderer sr;
    public List<Sprite> skins = new List<Sprite>();
    private int selectedSkin = 0;
    public GameObject playerskin;

    public void NextOption()
    {
        SoundManagerScript.playSound("click");
        selectedSkin = selectedSkin + 1;
        if (selectedSkin == skins.Count)
        {
            selectedSkin = 0;
        }
        sr.sprite = skins[selectedSkin];
    }
    public void BackOption()
    {
        SoundManagerScript.playSound("click");
        selectedSkin = selectedSkin - 1;
        if (selectedSkin < 0)
        {
            selectedSkin = skins.Count - 1;
        }
        sr.sprite = skins[selectedSkin];
    }

    public void PlayGame()
    {
        SoundManagerScript.playSound("click");
        #if UNITY_EDITOR
        PrefabUtility.SaveAsPrefabAsset(playerskin, "Assets/selectedskin.prefab");
        #endif
        
        SceneManager.LoadScene("Scene1");
    }

}

