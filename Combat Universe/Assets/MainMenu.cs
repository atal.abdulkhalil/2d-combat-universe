﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public void PlayGame() {
        SoundManagerScript.playSound("click");
        SceneManager.LoadScene(1);
    }

    public void QuitGame() {
        Application.Quit();
        SoundManagerScript.playSound("click");
        Debug.Log("game quit");
    }

    public void LoadRegister() {
        SoundManagerScript.playSound("click");
        SceneManager.LoadScene(3);
    }
}
