﻿using Cainos;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Fireball : MonoBehaviour
{
    public GameObject FireballImpact;
    public GameObject Sword;
    public GameObject Sword2;

    public List<GameObject> Weapons;

    public PixelCharacter PixelCharacter;
    public PixelCharacter PixelCharacter2;

    List<GameObject> Swords = new List<GameObject>();

    void OnTriggerEnter2D(Collider2D col)
    {
        /* Clone fire effect en sword for spawner */
        GameObject f = Instantiate(FireballImpact) as GameObject;
        f.transform.position = transform.position;
        GameObject s = Instantiate(Sword) as GameObject;
        s.transform.position = transform.position;

        /* Change weapon after been hit with Fire */
        if (col.gameObject.name == PixelCharacter.name || col.gameObject.name == PixelCharacter.Weapon.name)
        {
            if (this.gameObject.transform.position.y > col.transform.position.y)
            {
                Destroy(this.gameObject);
                Destroy(s);
            }
            PixelCharacter.AddWeapon(Sword2, true);
            //PixelCharacter.AddWeapon(Weapons[1], true);
        }
        else if (col.gameObject.name == PixelCharacter2.name || col.gameObject.name == PixelCharacter2.Weapon.name)
        {
            if (this.gameObject.transform.position.y > col.transform.position.y)
            {
                Destroy(this.gameObject);
                Destroy(s);
            }
            PixelCharacter2.AddWeapon(Sword2, true);
            //PixelCharacter2.AddWeapon(Weapons[1], true);
        }

        /* Delete Clone sword after 2 sec after hitting the ground */
        if (s.name == "Sword(Clone)")
        {
            Destroy(s, 10);
        }
    }

    /* Getting original sword in 5sec back after getting the "special" weapon */
    IEnumerator Delay(PixelCharacter character, GameObject weapon)
    {
        yield return new WaitForSeconds(5);
        character.AddWeapon(weapon, true);
    }
    void FixedUpdate()
    {
        if (PixelCharacter.Weapon.name == "Sword(Clone)")
        {
            StartCoroutine(Delay(PixelCharacter, Weapons[1]));
        }
        else if (PixelCharacter2.Weapon.name == "Sword(Clone)")
        {
            StartCoroutine(Delay(PixelCharacter2, Weapons[2]));
        }

        GameObject s = GameObject.Find("Sword(Clone)");
        if (s)
        {
            if ((PixelCharacter2.Weapon.name != "Sword(Clone)") && s.transform.position.y < -2 && s.transform.position.x <= (PixelCharacter.transform.position.x + 1) && s.transform.position.x >= (PixelCharacter.transform.position.x - 1))
            {
                PixelCharacter.AddWeapon(Sword2, true);
                Debug.Log(s.transform.position.y);
                Destroy(s);
            }
            else if ((PixelCharacter.Weapon.name != "Sword(Clone)") && s.transform.position.y < -2 && s.transform.position.x <= (PixelCharacter2.transform.position.x + 1) && s.transform.position.x >= (PixelCharacter2.transform.position.x - 1))
            {
                PixelCharacter2.AddWeapon(Sword2, true);
                Debug.Log(s.transform.position.y);
                Destroy(s);
            }
        }
    }
}




