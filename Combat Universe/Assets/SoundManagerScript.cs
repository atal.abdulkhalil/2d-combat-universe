﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManagerScript : MonoBehaviour
{
    public static AudioClip bodyHitSound;
    public static AudioClip damageSound;
    public static AudioClip dyingSound;
    public static AudioClip clickSound;
    static AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        bodyHitSound = Resources.Load<AudioClip>("bodyhit");
        damageSound = Resources.Load<AudioClip>("pain");
        dyingSound = Resources.Load<AudioClip>("dying");
        clickSound = Resources.Load<AudioClip>("click");
        audioSource = GetComponent<AudioSource>();
    }

    public static void playSound(string sound)
    {
        switch(sound) 
        {
          case "bodyhit":
            audioSource.PlayOneShot(bodyHitSound);
            break;

          case "pain":
            audioSource.PlayOneShot(damageSound);
            break;

          case "dying":
            audioSource.PlayOneShot(dyingSound);
            break;

          case "click":
            audioSource.PlayOneShot(clickSound);
            break;

        }
    }
}
