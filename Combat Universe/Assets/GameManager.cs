﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject selectedskin;
    public GameObject Playground;

    private Sprite Playgroundsprite;
    void Start()
    {
        Playgroundsprite = selectedskin.GetComponent<SpriteRenderer>().sprite;
        Playground.GetComponent<SpriteRenderer>().sprite = Playgroundsprite;
    }

}
