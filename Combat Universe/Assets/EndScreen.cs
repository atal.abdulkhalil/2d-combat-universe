﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class EndScreen : MonoBehaviour
{
    public void PlayGame()
    {
       	SceneManager.LoadScene(1);
    }

    public void ToMenu() 
    {
        SceneManager.LoadScene(0);
    }
}
